package com.enigma.exerciseone

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val ACTIVITY_NAME = "MainActivity"

    lateinit var buyFragment: BuyFragment
    lateinit var sellFragment: SellFragment
    lateinit var historyFragment: HistoryFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(ACTIVITY_NAME, "onCreate()")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buyFragment = BuyFragment()
        sellFragment = SellFragment()
        historyFragment = HistoryFragment()

        btn_buy.setOnClickListener(this)
        btn_history.setOnClickListener(this)
        btn_sell.setOnClickListener(this)
    }

    override fun onStart() {
        Log.i(ACTIVITY_NAME, "onStart()")
        super.onStart()
    }

    override fun onResume() {
        Log.i(ACTIVITY_NAME, "onResume()")
        super.onResume()
    }

    override fun onStop() {
        Log.i(ACTIVITY_NAME, "onStop()")
        super.onStop()
    }

    override fun onPause() {
        Log.i(ACTIVITY_NAME, "onPause()")
        super.onPause()
    }

    override fun onDestroy() {
        Log.i(ACTIVITY_NAME, "onDestroy()")
        super.onDestroy()
    }

    override fun onClick(v: View?) {
        Log.d("STRING", v.toString())
        when(v){
            btn_history -> {
                switchFragment(historyFragment)
            }
            btn_buy -> {
                switchFragment(buyFragment)
            }
            btn_sell -> {
                switchFragment(sellFragment)
            }
        }

    }

    private fun switchFragment(fragment: Fragment) {
        val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(fragment_container.id, fragment)
        fragmentTransaction.commit()
    }
}